import csv
import copy
from os import path
import pickle

# Program to download the category names of Google Scholar

import requests
from bs4 import BeautifulSoup

top_url = 'https://scholar.google.co.jp/citations?view_op=top_venues&hl=ja'
sub_url = top_url + '&vq={}'

def extract_categories(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    cs = soup.find_all('a', class_='gs_md_li')

    categories = []
    for c in cs:
        try:
            lbl = c.attrs['href']
            vq,vqval = lbl.split('?')[-1].split('&')[-1].split('=')
            if vq == 'vq':
                categories.append((vqval, str(c.contents[0])))
        except:
            pass

    return categories

def extract_sub_category(catkey='eng'):
    tree = {}
    sub_categories = dict(extract_categories(sub_url.format(catkey)))
    sub_categories.pop(catkey)
    for sub_catkey,name in sub_categories.items():
        tree[sub_catkey] = (name, extract_sub_sub_category(sub_catkey))
    return tree

def extract_sub_sub_category(sub_catkey='eng_robotics'):
    r = requests.get(sub_url.format(sub_catkey))
    soup = BeautifulSoup(r.text, 'html.parser')
    return [n.contents[0] for n in soup.find_all('td', class_='gsc_mvt_t')]

def get_category_definition(url=top_url):
    categories = dict(extract_categories(url)[:8])
    for catkey,name in categories.items():
        print('getting {} ...'.format(name))
        categories[catkey] = (name, extract_sub_category(catkey))
        
    with open('gss_category.def', 'wb') as f:
        pickle.dump(categories, f)
    return categories

def load_category_definition():
    with open('gss_category.def', 'rb') as f:
        return pickle.load(f)


import xlwings as xw
    
def convert_to_excel(category_def_file='gss_category.def'):
    '''
    Generate an Excel File from the pickled Google Scholar definition file
    '''
    def_tree = load_category_definition()

    book = xw.Book()
    wss = book.sheets
    for catkey,e in def_tree.items():
        catname,e2 = e
        print(catname)
        ws = wss.add(name=catkey, after=True)
        ws.cells[0,0].value = catname
        for j, (subcatkey,e) in enumerate(e2.items()):
            subcatname,e2 = e
            ws.cells[2,j].value = subcatname
            for i, gssname in enumerate(e2):
                ws.cells[3+i,j].value = gssname

    book.save('gss_categories.xlsx')


unit_names = ['AHFRC', 'AIRC', 'CPSEC', 'HARC', 'HIRI',
                  'ISRI', 'ITRI', 'RIRC', 'RWBC-OIL']

old_dir = 'gss20_1912'
new_dir = 'gss20_2004'

def load_csv(csv_file):
    records = []
    with open(csv_file, encoding='cp932') as f:
        reader = csv.DictReader(f)
        for row in reader:
            records.append(row)
    return records

def save_csv(records, filename):
    with open(filename, 'w', encoding='cp932') as f:
        writer = csv.DictWriter(f, records[0].keys())
        writer.writeheader()
        for r in records:
            writer.writerow(r)

def ids(records):
    return [record['受付番号'] for record in records]

def diff_records(unit_name):
    old_records = load_csv(path.join(old_dir, unit_name+'.csv'))
    new_records = load_csv(path.join(new_dir, unit_name+'.csv'))
    old_ids = [r['受付番号'] for r in old_records]
    diff_records = []
    for r in new_records:
        if not (r['受付番号'] in old_ids):
            diff_records.append(r)
    return diff_records

def info(records):
    return [(r['発表題目'],r['論文誌発行年月'],r['掲載誌名']) for r in records]


def create_inverse_table(gss_category_tree):
    inv_table = {}
    for gss_cat in gss_category_tree.items():
        _,gss_sub_tree = gss_category_tree[gss_cat]
        for gss_sub_cat in gss_sub_tree.keys():
            for journal in gss_sub_tree[gss_sub_cat]:
                inv_table[journal] = (gss_cat, gss_sub_cat)
    print('{} journals and conferences exist in GSS20'.format(len(inv_table.keys())))
    return inv_table


def create_inverse_table(gss_category_tree):
    inv_table = {}
    for cat_key,(cat_name, cat_tree) in gss_category_tree.items():
        for subcat_key,(subcat_name, subcat_list) in cat_tree.items():
            for publication in subcat_list:
                inv_table[publication] = (cat_key, subcat_key, cat_name, subcat_name)
    print('{} publication exists in GSS20'.format(len(inv_table.keys())))
    return inv_table

def check_gss20(records, inv_table):
    for r in records:
        try:
            n = r['GSS name']
            res = inv_table[n]
            r['GSS20'] = True
            r['GSS category'] = res[0]
            r['GSS subcategory'] = res[1]
            print('{} is in Google Top 20'.format(n))
        except KeyError:
            print('{} is Not'.format(n))

def update_gss20(inv_table, excel_file, sheet_name,
                 GSS_name_col=45, GSS20_col=46, GSS20_category_col=47, GSS20_subcategory_col=48):
    book = xw.Book(excel_file)
    s = book.sheets[sheet_name]
    i = 1
    while s.cells[i,GSS_name_col].value != None:
        try:
            name = s.cells[i,GSS_name_col].value
            res = inv_table[name]
            s.cells[i,GSS20_col].value = True
            s.cells[i,GSS20_category_col].value = res[0]
            s.cells[i,GSS20_subcategory_col].value = res[1]
            print('{} is in GSS Top 20'.format(name))        
        except KeyError:
            s.cells[i,GSS20_col].value = False
            print('{} is not in GSS Top 20'.format(name))        
        i = i + 1
            
import re

def main(excel_file='h5_gtop20（8月）.xlsx', sheet_name='2020年登録データ'):
    gss_category_tree = load_category_definition()
    inv_table = create_inverse_table(gss_category_tree)
    update_gss20(inv_table, excel_file, sheet_name)
