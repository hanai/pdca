import csv
import xlwings as xw

def load_csv(csv_file):
    records = []
    with open(csv_file, encoding='cp932') as f:
        reader = csv.DictReader(f)
        for row in reader:
            records.append(row)
    return records

def insert_records(headings_table, rs, st, start_row, existing_ids):
    inserted = 0
    for i,r in enumerate(rs):
        print('inserting ', i, 'th record')
        if r['受付番号'] in existing_ids:
            print(r['受付番号'], ' already exists')
        else:
            print('add new record ', r['受付番号'])
            for f in r.items():
                st.range((start_row + inserted, headings_table[f[0]])).value = f[1]
            inserted += 1

def read_headings(sheet):
    headings = {}
    j = 1
    while True:
        v = sheet.range((1, j)).value
        if v == None:
            break
        else:
            headings[v] = j
        j += 1
    return headings

def scan_existing_IDs(headings_table, st):
    ids = set()
    colnum = headings_table['受付番号']
    i = 1
    while True:
        v = st.range((i, colnum)).formula
        if v == '':
            break
        else:
            ids.add(v)
        i += 1
    return ids

def import_csv(source_csv_file,
               target_excel_file,
               target_sheet):
    data = load_csv(source_csv_file)
    wb = xw.Book(target_excel_file)
    st = wb.sheets[target_sheet]
    headings_table = read_headings(st)
    existing_ids = scan_existing_IDs(headings_table, st)

    # print(existing_ids)

    i = 1
    while st.range((i, 1)).value != None:
        i = i + 1

    print('start insertion from row ', i)
    insert_records(headings_table, data, st, i, existing_ids)

def import_csvs(csvs_dir='/Users/ryo/Documents/領域_作業用/PDCA定期集計/8月/raw_data',
                target_excel_file='h5_gtop20（8月）.xlsx',
                target_sheet='2020年登録データ'):
    for unit in ['AIRC', 'CPSEC', 'HARC', 'ICPS', 'HCMRC', 'HIIRI', 'dapc', 'pana-aaicrl', 'RWBC-OIL', 'AIST-CNRS', 'tico-al', 'is', 'tico-alcrl', 'hiri', 'itri', 'riric', 'ahf']:
        print('importing ', unit)
        try:
            import_csv('{}/{}.csv'.format(csvs_dir, unit),
                       target_excel_file=target_excel_file,
                       target_sheet=target_sheet)
        except FileNotFoundError as e:
            print(e)
